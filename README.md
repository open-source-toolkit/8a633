# CIFAR-100 图片格式数据集

## 简介

本仓库提供了一个CIFAR-100图片格式数据集，该数据集按照100个分类文件夹进行储存。CIFAR-100数据集是一个广泛使用的图像分类数据集，包含100个不同的类别，每个类别包含600张32x32像素的彩色图像。

## 数据集结构

数据集的结构如下：

```
CIFAR-100-dataset/
├── class1/
│   ├── image1.png
│   ├── image2.png
│   └── ...
├── class2/
│   ├── image1.png
│   ├── image2.png
│   └── ...
└── ...
```

每个文件夹代表一个类别，文件夹内包含该类别的所有图像。

## 使用方法

1. **克隆仓库**：
   ```bash
   git clone https://github.com/cyizhuo/CIFAR-100-dataset.git
   ```

2. **数据集路径**：
   克隆完成后，数据集将位于`CIFAR-100-dataset`目录下。

3. **加载数据**：
   您可以使用任何支持图像数据加载的深度学习框架（如TensorFlow、PyTorch等）来加载和使用该数据集。

## 参考链接

- [CIFAR-100官方网站](https://www.cs.toronto.edu/~kriz/cifar.html)

## 许可证

本数据集遵循CIFAR-100的原始许可证。请在使用前查阅相关许可证信息。

## 贡献

如果您有任何改进建议或发现了问题，欢迎提交Issue或Pull Request。

## 联系

如有任何问题，请联系仓库维护者：[cyizhuo](https://github.com/cyizhuo)。